package com.example.radiobutton;

import androidx.annotation.IdRes;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void makeEmotionToast (View view) {
        RadioGroup radioGroupEmotion = findViewById(R.id.RadioGroupAttendenace);
        RadioButton selectedRadioButton = findViewById(radioGroupEmotion.getCheckedRadioButtonId());

        String numberofStudents = selectedRadioButton.getText().toString();
        String emotionText = "";

        if (selectedRadioButton != null) {
            if (selectedRadioButton.equals(R.id.radioButtonHappy)) {
                emotionText = "Happy";
            } else if (selectedRadioButton.equals(R.id.radioButtonSad)) {
                emotionText = "Sad";
            } else if (selectedRadioButton.equals(R.id.radioButtonOk)) {
                emotionText = "Ok";
            }

        }

        Toast.makeText(this, emotionText, Toast.LENGTH_LONG);

    }
}